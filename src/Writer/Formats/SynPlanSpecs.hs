-----------------------------------------------------------------------------
-- |
-- Module      :  Writer.Formats.SynPlan
-- License     :  MIT (see the LICENSE file)
-- Maintainer  :  Alberto Camacho (acamacho@cs.toronto.edu)
-- 
-- Transforms a specification to the SynPlan format.
-- 
-----------------------------------------------------------------------------

module Writer.Formats.SynPlanSpecs where

-----------------------------------------------------------------------------

import Config
import Simplify

import Data.List
import Data.Char
import Data.LTL
import Data.Error
import Data.Specification

import Writer.Eval
import Writer.Data
import Writer.Utils

-----------------------------------------------------------------------------

-- | Synplan operator configuration.

opConfig
  :: OperatorConfig

opConfig = OperatorConfig
  { tTrue      = "true"
  , fFalse     = "false"
  , opNot      = UnaryOp  "!"   1
  , opAnd      = BinaryOp "&&"  4 AssocLeft
  , opOr       = BinaryOp "||"  4 AssocLeft
  , opImplies  = BinaryOpUnsupported
  , opEquiv    = BinaryOpUnsupported
  , opNext     = UnaryOp  "X"   1 
  , opFinally  = UnaryOp  "F"   1 
  , opGlobally = UnaryOp  "G"   1 
  , opUntil    = BinaryOp "U"   2 AssocLeft
  , opRelease  = BinaryOp "R"   3 AssocLeft
  , opWeak     = BinaryOpUnsupported
  }

-----------------------------------------------------------------------------

-- | SynPlan writer.

writeFormat
  :: Configuration -> Specification -> Either Error String

writeFormat c s = do
  (es,ss,rs,as,is,gs) <- eval c s
  fml0 <- merge es ss rs as is gs
  fml1 <- simplify (adjust c opConfig) $ noBooleanDerived fml0
  

  neg_initially <- mapM (simplify (adjust c opConfig) . Not . noBooleanDerived) es
  preset <- mapM (simplify (adjust c opConfig) . noBooleanDerived) ss
  
  require <- mapM (simplify (adjust c opConfig) . Globally . noBooleanDerived) rs
  assume <- mapM (simplify (adjust c opConfig) . noBooleanDerived) as
  
  neg_environment <- mapM (simplify (adjust c opConfig) . Not) (require ++ assume)

  assert <- mapM (simplify (adjust c opConfig) . Globally . noBooleanDerived) is
  guarantee <- mapM (simplify (adjust c opConfig) . noBooleanDerived) gs
  
  agent <- mapM (simplify (adjust c opConfig) ) (assert ++ guarantee)
  
  
  
  let
    neg_initially_2 = map (printFormula opConfig (outputMode c)) neg_initially
    preset_2 = map (printFormula opConfig (outputMode c)) preset
    neg_environment_2 = map (printFormula opConfig Fully) neg_environment
    agent_2 = map (printFormula opConfig (outputMode c)) agent


    neg_initially_output = map (\x -> "neg_initially " ++ x ++ ";") neg_initially_2
    preset_output = map (\x -> "preset " ++ x ++ ";") preset_2
    neg_environment_output = map (\x -> "neg_environment " ++ x ++ ";") neg_environment_2
    agent_output = map (\x -> "agent " ++ x ++ ";") agent_2
    
    
    neg_initially_output2 = map (++ "\n") neg_initially_output
    preset_output2 = map (++ "\n") preset_output
    neg_environment_output2 = map (++ "\n") neg_environment_output
    agent_output2 = map (++ "\n") agent_output

    ws = map (++ "\n") neg_initially_output2 ++ preset_output2 ++ neg_environment_output2 ++ agent_output2

    flatws = concat ws


  return $ flatws

  where
    noBooleanDerived fml = case fml of
      Implies x y -> Or $ map noBooleanDerived [Not x, y]
      Equiv x y   -> Or [ And $ map noBooleanDerived [x,y]
                       , And $ map noBooleanDerived [Not x, Not y]
                       ]
      _           -> applySub noBooleanDerived fml

-----------------------------------------------------------------------------


