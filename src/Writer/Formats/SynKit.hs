-----------------------------------------------------------------------------
-- |
-- Module      :  Writer.Formats.SynKit
-- License     :  MIT (see the LICENSE file)
-- Maintainer  :  Alberto Camacho (acamacho@cs.toronto.edu)
-- 
-- Transforms a specification to the SynKit format.
-- 
-----------------------------------------------------------------------------

module Writer.Formats.SynKit where

-----------------------------------------------------------------------------

import Config
import Simplify

import Data.List
import Data.Char
import Data.LTL
import Data.Error
import Data.Specification

import Writer.Eval
import Writer.Data
import Writer.Utils

-----------------------------------------------------------------------------

-- | SynKit operator configuration.

opConfig
  :: OperatorConfig

opConfig = OperatorConfig
  { tTrue      = "true"
  , fFalse     = "false"
  , opNot      = UnaryOp  "!"   1
  , opAnd      = BinaryOp "&&"  4 AssocLeft
  , opOr       = BinaryOp "||"  4 AssocLeft
  , opImplies  = BinaryOpUnsupported
  , opEquiv    = BinaryOpUnsupported
  , opNext     = UnaryOp  "X"   1 
  , opFinally  = UnaryOp  "F"   1 
  , opGlobally = UnaryOp  "G"   1 
  , opUntil    = BinaryOp "U"   2 AssocLeft
  , opRelease  = BinaryOp "R"   3 AssocLeft
  , opWeak     = BinaryOpUnsupported
  }

-----------------------------------------------------------------------------

-- | SynKit writer.

writeFormat
  :: Configuration -> Specification -> Either Error String

writeFormat c s = do
  (es,ss,rs,as,is,gs) <- eval c s
  fml0 <- merge es ss rs as is gs
  fml1 <- simplify (adjust c opConfig) $ noBooleanDerived fml0
  

  initially <- mapM (simplify (adjust c opConfig) . noBooleanDerived) es
  preset <- mapM (simplify (adjust c opConfig) . noBooleanDerived) ss
  
  require <- mapM (simplify (adjust c opConfig) . Globally . noBooleanDerived) rs
  assume <- mapM (simplify (adjust c opConfig) . noBooleanDerived) as
  
  environment <- mapM (simplify (adjust c opConfig)) (require ++ assume)

  assert <- mapM (simplify (adjust c opConfig) . Globally . noBooleanDerived) is
  guarantee <- mapM (simplify (adjust c opConfig) . noBooleanDerived) gs
  
  system <- mapM (simplify (adjust c opConfig) ) (assert ++ guarantee)
  
  
  
  let
    initially_2 = map (printFormula opConfig (outputMode c)) initially
    preset_2 = map (printFormula opConfig (outputMode c)) preset
    environment_2 = map (printFormula opConfig Fully) environment
    system_2 = map (printFormula opConfig (outputMode c)) system


    initially_output = map (\x -> "initially " ++ x ++ ";") initially_2
    preset_output = map (\x -> "preset " ++ x ++ ";") preset_2
    environment_output = map (\x -> "environment " ++ x ++ ";") environment_2
    system_output = map (\x -> "system " ++ x ++ ";") system_2
    
    
    initially_output2 = map (++ "\n") initially_output
    preset_output2 = map (++ "\n") preset_output
    environment_output2 = map (++ "\n") environment_output
    system_output2 = map (++ "\n") system_output

    ws = map (++ "\n") initially_output2 ++ preset_output2 ++ environment_output2 ++ system_output2

    flatws = concat ws


  return $ flatws

  where
    noBooleanDerived fml = case fml of
      Implies x y -> Or $ map noBooleanDerived [Not x, y]
      Equiv x y   -> Or [ And $ map noBooleanDerived [x,y]
                       , And $ map noBooleanDerived [Not x, Not y]
                       ]
      _           -> applySub noBooleanDerived fml

-----------------------------------------------------------------------------


