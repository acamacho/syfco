-----------------------------------------------------------------------------
-- |
-- Module      :  Writer.Formats.SynPlan
-- License     :  MIT (see the LICENSE file)
-- Maintainer  :  Alberto Camacho (acamacho@cs.toronto.edu)
-- 
-- Transforms a specification to the SynPlan format.
-- 
-----------------------------------------------------------------------------

module Writer.Formats.SynPlan where

-----------------------------------------------------------------------------

import Config
import Simplify

import Data.List
import Data.Char
import Data.LTL
import Data.Error
import Data.Specification

import Writer.Eval
import Writer.Data
import Writer.Utils

-----------------------------------------------------------------------------

-- | Synplan operator configuration.

opConfig
  :: OperatorConfig

opConfig = OperatorConfig
  { tTrue      = "true"
  , fFalse     = "false"
  , opNot      = UnaryOp  "!"   1
  , opAnd      = BinaryOp "&&"  4 AssocLeft
  , opOr       = BinaryOp "||"  4 AssocLeft
  , opImplies  = BinaryOpUnsupported
  , opEquiv    = BinaryOpUnsupported
  , opNext     = UnaryOp  "X"   1 
  , opFinally  = UnaryOp  "F"   1 
  , opGlobally = UnaryOp  "G"   1 
  , opUntil    = BinaryOp "U"   2 AssocLeft
  , opRelease  = BinaryOp "R"   3 AssocLeft
  , opWeak     = BinaryOpUnsupported
  }

-----------------------------------------------------------------------------

-- | SynPlan writer.

writeFormat
  :: Configuration -> Specification -> Either Error String

writeFormat c s = do
  (es,ss,rs,as,is,gs) <- eval c s
  fml0 <- merge es ss rs as is gs
  fml1 <- simplify (adjust c opConfig) $ noBooleanDerived fml0

  return $ printFormula opConfig (outputMode c) fml1

  where
    noBooleanDerived fml = case fml of
      Implies x y -> Or $ map noBooleanDerived [Not x, y]
      Equiv x y   -> Or [ And $ map noBooleanDerived [x,y]
                       , And $ map noBooleanDerived [Not x, Not y]
                       ]
      _           -> applySub noBooleanDerived fml

-----------------------------------------------------------------------------


